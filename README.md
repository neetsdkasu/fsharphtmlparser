F# HTML Parser 
==============

Development environment   
-----------------------
 - CPU: Intel Atom N570 1.66 GHz  
 - Memory: 1.0 GB  
 - OS: Windows 7 Starter SP1  
 - .NET Framework: v4.0.30319 & v4.5.51209  
 - Compiler: F# 2.0 (build 4.0.40219.1)  
 - Make tool: Borland MAKE Version 5.2  
 